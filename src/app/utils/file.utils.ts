import * as FileSaver from 'file-saver';

export class FileUtils {
	static appendText(textBase: string[], textToAppend: string[]): string[] {
		textBase.push(...textToAppend);
		return textBase;
	}

	/**
	 * Salva um texto em arquivo a ser baixado
	 * Cada linha do texto deve ser um item do array
	 */
	static writeTextToFile(text: string[]): void {
		const blob = new Blob([text.join('\n')], {
			type: 'text/plain;charset=utf-8',
		});
		FileSaver.saveAs(blob);
	}

	static workHeader(): string[] {
		return [
			'Trabalho de Organização de Computadores',
			'\n',
			'Professor: Marcelo Lobosco',
			'Integrantes: Guilherme e Helder',
			'---------------------------------------',
		];
	}
}
