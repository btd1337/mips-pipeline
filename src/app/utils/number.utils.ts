import { BINARY_BASE } from '../app.constants';

export class NumberUtils {
	/**
	 * Converte número decimal para formato binário
	 * @param decimal
	 * @returns
	 */
	static decimalToBinary(decimal: number): string {
		return decimal.toString(BINARY_BASE);
	}

	/**
	 * Converte número em formato binário para decimal
	 * @param binary
	 * @returns
	 */
	static binaryToDecimal(binary: string): number {
		return parseInt(binary, 2);
	}
}
