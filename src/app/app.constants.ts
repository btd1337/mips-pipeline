/**
 * Tamanho da palavra em bits
 */
export const WORD_SIZE = 8;

/**
 * Num de bits do processador
 */
export const NUM_BITS_PROCESSOR = 32;

/**
 * Número de palavras necessárias para obter a instrução completa
 */
export const WORDS_IN_INSTRUCTION = NUM_BITS_PROCESSOR / WORD_SIZE;

export const BINARY_BASE = 2;

export const NUM_STAGES = 5;

export const NUM_SIGNALS = 10;

export const OPCODE_START = 0;

export const REGISTER_1_START = 6;

export const REGISTER_2_START = 11;

export const REGISTER_3_START = 16;

export const SHAMT_START = 21;

export const FUNCT_START = 26;
