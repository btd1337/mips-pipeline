import { WORDS_IN_INSTRUCTION } from '../app.constants';
import { MemoryAddress } from './memory-address.model';

export class Memory {
	addresses: Array<MemoryAddress> = [];

	memorySize: number; // Bytes

	constructor(memorySize: number) {
		this.memorySize = memorySize;
		this.initMemory(memorySize);
	}

	initMemory(memorySize: number): void {
		this.addresses = new Array<MemoryAddress>();
		for (let i = 0; i < memorySize; i++) {
			this.addresses.push(new MemoryAddress(i.toString(), i.toString()));
		}
	}

	/**
	 * Obtém a instrução dado um endereço
	 * @param baseAddress endereço base
	 * @returns instrução completa
	 */
	getInstructionAt(baseAddress: number): string {
		let instruction = '';
		for (let i = 0; i < WORDS_IN_INSTRUCTION; i++) {
			instruction += this.addresses[baseAddress + i].value;
		}

		return instruction;
	}

	toString(): string {
		const text: string[] = [];
		this.addresses.forEach((data) => text.push(data.toString()));
		return text.join('\n');
	}
}
