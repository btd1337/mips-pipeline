import { Register } from './register.model';

export class RegisterFile {
	numRegisters: number; // bits

	// http://www.cs.uwm.edu/classes/cs315/Bacon/Lecture/HTML/ch05s03.html#mips_regs_convention
	registerNames = [
		'$zero',
		'$at',
		'$v0',
		'$v1',
		'$a0',
		'$a1',
		'$a2',
		'$a3',
		'$t0',
		'$t1',
		'$t2',
		'$t3',
		'$t4',
		'$t5',
		'$t6',
		'$t7',
		'$s0',
		'$s1',
		'$s2',
		'$s3',
		'$s4',
		'$s5',
		'$s6',
		'$s7',
		'$t8',
		'$t9',
		'$k0',
		'$k1',
		'$gp',
		'$sp',
		'$fp',
		'$ra',
	];

	registers: Array<Register> = [];

	constructor(numRegisters: number) {
		this.numRegisters = numRegisters;
		this.initRegisters(numRegisters);
	}

	initRegisters(numRegisters: number): void {
		this.registers = new Array<Register>();
		for (let i = 0; i < numRegisters; i++) {
			this.registers.push(new Register(i.toString(), this.registerNames[i]));
		}
	}

	toString(): string {
		const text: string[] = [];
		this.registers.forEach((data) => text.push(data.toString()));
		return text.join('\n');
	}
}
