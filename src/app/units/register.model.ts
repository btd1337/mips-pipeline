export class Register {
	id: string;

	name: string;

	value: string;

	constructor(id: string, name: string) {
		this.id = id;
		this.name = name;
		this.value = '00000000000000000000000000000000';
	}

	toString(): string {
		const maxRegisterNameLength = 5;
		return `${
			this.name.length === maxRegisterNameLength
				? this.name
				: '  '.concat(this.name)
		}: ${this.value}`;
	}
}
