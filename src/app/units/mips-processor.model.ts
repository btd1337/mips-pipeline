import { InstructionMemory } from './instruction-memory.model';
import { RegisterFile } from './register-file.model';
import { ControlUnit } from './control-unit.model';
import { ProgramCounter } from './program-counter.model';
import {
	FUNCT_START,
	NUM_BITS_PROCESSOR,
	NUM_STAGES,
	REGISTER_1_START,
	REGISTER_2_START,
	REGISTER_3_START,
	SHAMT_START,
	WORDS_IN_INSTRUCTION,
	WORD_SIZE,
} from '../app.constants';
import { Register } from './register.model';
import { ALU } from './alu.class';
import { ALUControl } from './alu-control.class';
import { DataMemory } from './data-memory.model';
import { NumberUtils } from '../utils/number.utils';
import { SignExtend } from './sign-extend.class';
import { Multiplexer } from './multiplexer.class';
import { Adder } from './adder.model';
import { FileUtils } from '../utils/file.utils';

export class MipsProcessor {
	instructionMemorySize: number;

	dataMemorySize: number;

	numRegisters: number;

	pc: ProgramCounter;

	instructionMemory: InstructionMemory;

	registerFile: RegisterFile;

	controlUnit: ControlUnit;

	branchAdder: Adder;

	aluControl: ALUControl;

	multiplexers: Array<Multiplexer>;

	alu: ALU;

	dataMemory: DataMemory;

	checkpointRegisters: Array<Register>;

	instructionsByStage: string[];

	currentClock: number;

	logs: string[];

	constructor(
		instructionMemorySize: number,
		dataMemorySize: number,
		numRegisters: number,
	) {
		this.instructionMemorySize = instructionMemorySize;
		this.dataMemorySize = dataMemorySize;
		this.numRegisters = numRegisters;
		this.pc = new ProgramCounter();
		this.instructionMemory = new InstructionMemory(instructionMemorySize);
		this.dataMemory = new DataMemory(dataMemorySize);
		this.registerFile = new RegisterFile(numRegisters);
		this.controlUnit = new ControlUnit();
		this.branchAdder = new Adder();
		this.aluControl = new ALUControl();
		this.alu = new ALU();
		this.instructionsByStage = new Array(NUM_STAGES).fill(' ');
		this.logs = [];

		const multiplexerNames = ['ALUSrc', 'RegDst', 'MemtoReg', 'PCSrc'];
		this.multiplexers = new Array<Multiplexer>(multiplexerNames.length);
		for (let i = 0; i < multiplexerNames.length; i++) {
			this.multiplexers[i] = new Multiplexer(multiplexerNames[i]);
		}

		const registerNames = ['IF/ID', 'ID/EX', 'EX/MEM', 'MEM/WB'];
		this.checkpointRegisters = new Array<Register>();
		for (let i = 0; i < NUM_STAGES - 1; i++) {
			this.checkpointRegisters.push(
				new Register(i.toString(), registerNames[i]),
			);
		}

		this.currentClock = 1;

		// Deveria 'runar' enquanto tiver instruções
		// Ou se for execução passo-a-passo enquanto o usuário pressionar o botão de passo-a-passo
	}

	run(): boolean {
		return this.clock();
	}

	private clock(): boolean {
		if (this.instructionsByStage[0] !== ' ') {
			this.stage1();
			this.recordLogs(1);
		}

		if (this.instructionsByStage[1] !== ' ') {
			this.stage2();
			this.recordLogs(2);
		}

		if (this.instructionsByStage[2] !== ' ') {
			this.stage3();
			this.recordLogs(3);
		}

		if (this.instructionsByStage[3] !== ' ') {
			this.stage4();
			this.recordLogs(4);
		}

		if (this.instructionsByStage[4] !== ' ') {
			this.stage5();
			this.recordLogs(5);
		}

		this.updateInstructionsByStages(); // Deve ser chamado após o último stage executar
		this.currentClock++;

		return this.hasValidInstruction();
	}

	// Pipeline dividido em cinco estágios
	private stage1(): void {
		// A instrução deveria ir para o registrador 'IF/ID'
		const stageInstruction = this.instructionsByStage[0]; // Pegando instrução na IM

		// adiciona a instrução atual, na posição inicial do array de instruções executando
		this.instructionsByStage[0] = stageInstruction;

		this.checkpointRegisters[0].value = stageInstruction; // Colocando a instrução em IF/ID

		this.controlUnit.calculateControlSignals(stageInstruction); // Calculando sinais de controle
		this.pc.increment(); // Atualizando PC (sem contar desvio)
	}

	private stage2(): void {
		// Deveria ler as instruções do banco de registradores para mandá-las para a ALU
		// Salvar em ID/EX os registradores a serem operados e o possível desvio a ser tomado (15-0)

		this.checkpointRegisters[1].value = this.checkpointRegisters[0].value;
		// console.log('ID/EX', this.checkpointRegisters[1].value);

		// Atualizando multiplexador de RegDst
		this.multiplexers[1].receiveData(
			this.checkpointRegisters[1].value.substring(
				REGISTER_2_START,
				REGISTER_3_START,
			),
			this.checkpointRegisters[1].value.substring(
				REGISTER_3_START,
				SHAMT_START,
			),
		);

		// 15 bits finais da instrução extendidos
		const extendedValue = SignExtend.extendNumber(
			this.checkpointRegisters[1].value.substring(
				REGISTER_3_START,
				NUM_BITS_PROCESSOR,
			),
		);

		this.multiplexers[0].receiveEntry1(extendedValue); // Mandando valor extendido para mux
		this.branchAdder.receiveData(
			extendedValue,
			NumberUtils.decimalToBinary(this.pc.value),
		);
	}

	private stage3(): void {
		// ALUControl recebe sinais de controle para determinar operação
		this.aluControl.receiveControlSignals(
			this.controlUnit.controlSignals[7].value,
			this.controlUnit.controlSignals[8].value,
			this.checkpointRegisters[1].value.substring(
				FUNCT_START,
				NUM_BITS_PROCESSOR,
			),
			this.checkpointRegisters[1].value.substring(0, REGISTER_1_START),
		);

		// Calculando endereço dos registradores na instrução
		const register1Index = NumberUtils.binaryToDecimal(
			this.checkpointRegisters[1].value.substring(
				REGISTER_1_START,
				REGISTER_2_START,
			),
		);
		const register2Index = NumberUtils.binaryToDecimal(
			this.checkpointRegisters[1].value.substring(
				REGISTER_2_START,
				REGISTER_3_START,
			),
		);
		const entry1 = this.registerFile.registers[register1Index].value;

		// Passa registrador 2 da instrução e offset para o multiplexador decidir qual usar
		this.multiplexers[0].receiveEntry0(
			this.registerFile.registers[register2Index].value,
		);

		// ALU recebe operadores
		this.alu.receiveData(
			entry1,
			this.multiplexers[0].chooseValue(
				this.controlUnit.controlSignals[5].value,
			),
			this.aluControl.operation,
		);
		if (this.alu.operation === 'sll') {
			this.alu.entry2 = this.checkpointRegisters[1].value.substring(
				SHAMT_START,
				FUNCT_START,
			);
		}

		// Executando operação na ALU
		const result = this.alu.execute();
		this.multiplexers[2].receiveEntry0(result); // Mandar resultado para multiplexador 'MemtoReg'
		this.checkpointRegisters[2].value = result; // Caso seja lw/sw, o resultado será o offset na DataMemory

		// Adicionando valores no mux de PCSrc
		this.multiplexers[3].receiveData(
			NumberUtils.decimalToBinary(this.pc.value),
			this.branchAdder.add(),
		);

		// O valor de pc só é alterado se o zero da ALU estiver ativo
		// e se o valor do sinal PCSrc for 1
		if (this.alu.zero) {
			this.pc.value = NumberUtils.binaryToDecimal(
				this.multiplexers[3].chooseValue(
					this.controlUnit.controlSignals[6].value,
				),
			);
		}
	}

	private stage4(): void {
		// Escrita e Leitura na memória de Dados para instruções lw e sw
		if (this.controlUnit.controlSignals[0].value === 1) {
			// lw, com MemRead ativo
			const offset = this.checkpointRegisters[2].value;
			const value = this.dataMemory.getInstructionAt(
				NumberUtils.binaryToDecimal(offset),
			);
			this.multiplexers[2].receiveEntry1(value);
		}
		if (this.controlUnit.controlSignals[1].value === 1) {
			// sw, com MemWrite ativo
			const offset = NumberUtils.binaryToDecimal(
				this.checkpointRegisters[2].value,
			);
			const registerPos = NumberUtils.binaryToDecimal(
				this.checkpointRegisters[1].value.substring(
					REGISTER_1_START,
					REGISTER_2_START,
				),
			);
			for (let i = offset; i < WORDS_IN_INSTRUCTION + offset; i++) {
				this.dataMemory.addresses[i].value = this.registerFile.registers[
					registerPos
				].value.substring(
					(i - offset) * WORD_SIZE,
					(i - offset) * WORD_SIZE + WORD_SIZE,
				);
			}
		}
	}

	private stage5(): void {
		// Escrita no banco de registradores
		// Se RegWrite estiver ativo
		if (this.controlUnit.controlSignals[4].value === 1) {
			// Calculando endereço do registrador a ser escrito baseado no sinal RegDst
			const regDstAddressBinary = this.multiplexers[1].chooseValue(
				this.controlUnit.controlSignals[3].value,
			);
			const regDstAddress = NumberUtils.binaryToDecimal(regDstAddressBinary);

			// O registrador será escrito com o valor no multiplexador MemtoReg
			this.registerFile.registers[regDstAddress].value =
				this.multiplexers[2].chooseValue(
					this.controlUnit.controlSignals[2].value,
				);
		}
	}

	/**
	 * Atualiza qual é a instrução que está em cada stage após cada um ter sido executado
	 * Usado para monociclo
	 */
	private updateInstructionsByStages(): void {
		this.instructionsByStage[0] = this.instructionMemory.getInstructionAt(
			this.pc.value,
		);

		if (this.instructionsByStage[0] === '00000000000000000000000000000000') {
			this.instructionsByStage[0] = ' ';
		}

		for (let i = NUM_STAGES - 1; i > 0; i--) {
			this.instructionsByStage[i] = this.instructionsByStage[0];
		}
	}

	/**
	 * Atualiza qual é a instrução que está em cada stage após cada um ter sido executado
	 * Era pra ser usado se fosse com Pipeline
	 */
	/*
	private updateInstructionsByStages(isStarted = true): void {
		if (isStarted) {
			for (let i = NUM_STAGES - 1; i > 0; i--) {
				this.instructionsByStage[i] = this.instructionsByStage[i - 1];
			}
		}

		this.instructionsByStage[0] = this.instructionMemory.getInstructionAt(
			this.pc.value,
		);

		if (this.instructionsByStage[0] === '00000000000000000000000000000000') {
			this.instructionsByStage[0] = ' ';
		}
	}
	*/

	loadInstructionsFromFile(instructions: string[]): void {
		if (instructions.length > this.instructionMemorySize) {
			throw new Error('Limite de memória excedido');
		}

		for (let i = 0; i < instructions.length; i++) {
			for (let j = 0; j < WORDS_IN_INSTRUCTION; j++) {
				const initStringPosition = WORD_SIZE * j;

				this.instructionMemory.addresses[i * WORDS_IN_INSTRUCTION + j].value =
					instructions[i].substring(
						initStringPosition,
						initStringPosition + WORD_SIZE,
					);
			}
		}

		this.updateInstructionsByStages();
	}

	private recordLogs(stage: number): void {
		const text: string[] = [
			`\n\n= Estágio ${stage} =\n\n`,
			`Ciclo do clock: ${this.currentClock}`,
			`PC: ${this.pc.value}`,
			`Instrução: ${this.instructionsByStage[stage - 1]}`,
			`\nSinais de Controle\n`,
			this.controlUnit.toString(),
			`\nBanco de Registradores:\n`,
			this.registerFile.toString(),
			`\nMemória de Dados\n`,
			this.dataMemory.toString(),
		];
		FileUtils.appendText(this.logs, text);
	}

	/**
	 * Retorna se tem instrução executando em algum estágio
	 * @returns
	 */
	private hasValidInstruction(): boolean {
		for (const instruction of this.instructionsByStage) {
			if (instruction !== ' ') {
				return true;
			}
		}
		return false;
	}
}
