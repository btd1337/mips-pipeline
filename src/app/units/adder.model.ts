import { NumberUtils } from '../utils/number.utils';

export class Adder {
	entry0: string;

	entry1: string;

	constructor() {
		this.entry0 = '';
		this.entry1 = '';
	}

	receiveData(entry0: string, entry1: string): void {
		this.entry0 = entry0;
		this.entry1 = entry1;
	}

	add(): string {
		const n1 = NumberUtils.binaryToDecimal(this.entry0);
		const n2 = NumberUtils.binaryToDecimal(this.entry1);
		const result = n1 + n2;
		return NumberUtils.decimalToBinary(result);
	}
}
