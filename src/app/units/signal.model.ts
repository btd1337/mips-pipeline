export class Signal {
	name: string;

	value: number;

	constructor(name: string, value: number) {
		this.name = name;
		this.value = value;
	}

	toString(): string {
		return `${this.name}: ${this.value}`;
	}
}
