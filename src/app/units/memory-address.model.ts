export class MemoryAddress {
	id: string;

	address: string;

	value: string;

	constructor(id: string, address: string) {
		this.id = id;
		this.address = address;
		this.value = '00000000';
	}

	toString(): string {
		return `${this.address}: ${this.value}`;
	}
}
