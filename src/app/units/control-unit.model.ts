import { NUM_SIGNALS, OPCODE_START, REGISTER_1_START } from '../app.constants';
import { Signal } from './signal.model';

export class ControlUnit {
	controlSignals: Array<Signal>;

	constructor() {
		this.controlSignals = new Array<Signal>(NUM_SIGNALS);
		this.controlSignals[0] = new Signal('MemRead', 0);
		this.controlSignals[1] = new Signal('MemWrite', 0);
		this.controlSignals[2] = new Signal('MemtoReg', 0);
		this.controlSignals[3] = new Signal('RegDst', 0);
		this.controlSignals[4] = new Signal('RegWrite', 0);
		this.controlSignals[5] = new Signal('ALUSrc', 0);
		this.controlSignals[6] = new Signal('PCSrc', 0);
		this.controlSignals[7] = new Signal('ALUOp1', 0);
		this.controlSignals[8] = new Signal('ALUOp0', 0);
		this.controlSignals[9] = new Signal('Jump', 0);
	}

	calculateControlSignals(instruction: string): void {
		const opcode = instruction.substring(OPCODE_START, REGISTER_1_START);
		// const funct = instruction.substring(26, 32);

		for (let i = 0; i < this.controlSignals.length; i++) {
			this.controlSignals[i].value = 0;
		}

		// jr opcode-000000 rs(registrador que armazena próximo endereço de pc) 0..0 funct-001000

		// Tipo-R opcode-000000 rs rt rd shamt funct-xxxxxx https://www.d.umn.edu/~gshute/mips/rtype.xhtml
		// add funct-100000
		// and funct-100100
		// sub funct-100010
		// sll funct-000000
		// or funct-100101
		// slt funct-101010

		// Tipo-I opcode-xxxxxx rs rd mm https://www.d.umn.edu/~gshute/mips/itype.xhtml
		// addi opcode-001000
		// beq opcode-000100
		// bne opcode-000101
		// lw opcode-100011
		// sw opcode-101011

		// Tipo-J opcode-xxxxxx target https://www.d.umn.edu/~gshute/mips/jtype.xhtml
		// j opcode-000010
		// jal opcode-000011

		// Como tudo está 0, só precisa mudar certos sinais para 1
		switch (opcode) {
			case '000000': {
				// console.log('Instrução do Tipo-R');
				// tudo 0, menos RegDst, RegWrite e ALUOp1
				this.controlSignals[3].value = 1;
				this.controlSignals[4].value = 1;
				this.controlSignals[7].value = 1;
				break;
				// A ALU deve analisar o campo funct de instruções R, para determinar a operação a ser feita
			}
			case '001000': {
				// console.log('Instrução addi');
				// tudo 0, menos RegWrite, ALUSrc e ALUOp1
				this.controlSignals[4].value = 1;
				this.controlSignals[5].value = 1;
				this.controlSignals[7].value = 1;
				// Já que o addi é somar, estou levando em conta
				// que o addi tem os mesmos sinais enviados para a ALU do que o add
				break;
			}
			case '000100': {
				// console.log('Instrução beq');
				// tudo 0, menos PCSrc e ALUOp0
				this.controlSignals[6].value = 1;
				this.controlSignals[8].value = 1;
				break;
			}
			case '000101': {
				// console.log('Instrução bne');
				// tudo 0, menos PCSrc e ALUOp0
				this.controlSignals[6].value = 1;
				this.controlSignals[8].value = 1;
				// A ALU vai talvez ter que analisar o opcode ou o funct
				// para determinar se é beq ou bne, talvez tenha que mudar algo aqui
				break;
			}
			case '100011': {
				// console.log('Instrução lw');
				// tudo 1, menos MemWrite, RegDst e PCSrc
				this.controlSignals[0].value = 1;
				this.controlSignals[2].value = 1;
				this.controlSignals[4].value = 1;
				this.controlSignals[5].value = 1;
				break;
			}
			case '101011': {
				// console.log('Instrução sw');
				// tudo 0, menos MemWrite e ALUSrc
				this.controlSignals[1].value = 1;
				this.controlSignals[5].value = 1;
				break;
			}
			case '000010': {
				// console.log('Instrução j');
				// tudo 0, menos jump
				this.controlSignals[9].value = 1;
				break;
			}
			case '000011': {
				// console.log('Instrução jal');
				// jal, desvia para endereço e simultaneamente salva
				// endereço da instrução seguinte(PC + 4) no registrador $ra
				break;
			}
			default: {
				// TODO: Encerrar a aplicação
				throw new Error('Instrução não reconhecida');
			}
		}

		// Para printar conteúdo dos sinais de controle
		// this.controlSignals.forEach((signal) => {
		// 	console.log(`${signal.name}: ${signal.active}`);
		// });
	}

	toString(): string {
		const text: string[] = [];
		this.controlSignals.forEach((data) => text.push(data.toString()));
		return text.join('\n');
	}
}
