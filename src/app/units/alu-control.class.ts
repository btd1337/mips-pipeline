export class ALUControl {
	operation: string;

	constructor() {
		this.operation = '';
	}

	receiveControlSignals(
		op1: number,
		op0: number,
		funct: string,
		opcode: string,
	): void {
		// ALUControl recebe sinais para determinar operação a ser executada na ALU
		// São dois sinais ALUOp
		// Para lw/sw, ALUOp = 00
		// Para beq/bne, ALUOp = 01
		// Para tipo-r, ALUOp = 10
		// Mas para tipo-r é preciso mais informaçãoes, obtidas no campo funct
		// Após verificar qual operação ser feita, salvar isso em operation
		if (op1 === 0 && op0 === 0) {
			this.operation = 'lw/sw';
		} else if (op1 === 0 && op0 === 1) {
			if (opcode === '000100') {
				this.operation = 'beq';
			} else {
				this.operation = 'bne';
			}
		} else {
			if (opcode === '001000') {
				this.operation = 'addi';
			}
			// tipo-r
			switch (funct) {
				case '100000': {
					this.operation = 'add';
					break;
				}
				case '100010': {
					this.operation = 'sub';
					break;
				}
				case '100100': {
					this.operation = 'and';
					break;
				}
				case '100101': {
					this.operation = 'or';
					break;
				}
				case '101010': {
					this.operation = 'slt';
					break;
				}
				case '000000': {
					this.operation = 'sll';
					break;
				}
				default: {
					break;
				}
			}
		}
	}
}
