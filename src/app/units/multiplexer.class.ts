export class Multiplexer {
	entry0: string;

	entry1: string;

	name: string;

	constructor(name: string) {
		this.entry0 = '';
		this.entry1 = '';
		this.name = name;
	}

	receiveEntry0(entry0: string): void {
		this.entry0 = entry0;
	}

	receiveEntry1(entry1: string): void {
		this.entry1 = entry1;
	}

	receiveData(entry0: string, entry1: string): void {
		this.entry0 = entry0;
		this.entry1 = entry1;
	}

	chooseValue(signal: number): string {
		if (signal === 0) {
			return this.entry0;
		}
		return this.entry1;
	}
}
