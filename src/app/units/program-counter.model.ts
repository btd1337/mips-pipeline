export class ProgramCounter {
	private _value: number;

	constructor(value = 0) {
		this._value = value;
	}

	get value(): number {
		return this._value;
	}

	set value(value: number) {
		this._value = value;
	}

	/**
	 * Aponta o PC para o próximo endereço de memória
	 * 4 * 8bits = 32bits
	 */
	increment(): void {
		this._value += 4;
	}
}
