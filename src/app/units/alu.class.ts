import { NUM_BITS_PROCESSOR } from '../app.constants';
import { NumberUtils } from '../utils/number.utils';

export class ALU {
	entry1: string;

	entry2: string;

	operation: string;

	result: string;

	zero: boolean;

	constructor() {
		this.entry1 = '';
		this.entry2 = '';
		this.operation = '';
		this.result = '';
		this.zero = false;
	}

	receiveData(entry1: string, entry2: string, operation: string): void {
		this.entry1 = entry1;
		this.entry2 = entry2;
		this.operation = operation;
	}

	execute(): string {
		switch (this.operation) {
			case 'add': {
				const n1 = NumberUtils.binaryToDecimal(this.entry1);
				const n2 = NumberUtils.binaryToDecimal(this.entry2);
				const num = n1 + n2;
				const aux = NumberUtils.decimalToBinary(num);
				this.result = '';
				for (let i = NUM_BITS_PROCESSOR; i > aux.length; i--) {
					this.result += '0';
				}
				this.result += aux;
				break;
			}
			case 'sub': {
				const n1 = NumberUtils.binaryToDecimal(this.entry1);
				const n2 = NumberUtils.binaryToDecimal(this.entry2);
				const num = n1 - n2;
				const aux = NumberUtils.decimalToBinary(num);
				this.result = '';
				for (let i = NUM_BITS_PROCESSOR; i > aux.length; i--) {
					this.result += '0';
				}
				this.result += aux;
				break;
			}
			case 'and': {
				let result = '';
				for (let i = 0; i < this.entry1.length; i++) {
					if (this.entry1.charAt(i) === '1' && this.entry2.charAt(i) === '1') {
						result += '1';
					} else {
						result += '0';
					}
				}
				this.result = result;
				break;
			}
			case 'or': {
				let result = '';
				for (let i = 0; i < this.entry1.length; i++) {
					if (this.entry1.charAt(i) === '1' || this.entry2.charAt(i) === '1') {
						result += '1';
					} else {
						result += '0';
					}
				}
				this.result = result;
				break;
			}
			case 'slt': {
				// Usando zero da ALU, mas n sei como usar o zero da ALU
				const n1 = NumberUtils.binaryToDecimal(this.entry1);
				const n2 = NumberUtils.binaryToDecimal(this.entry2);
				if (n1 < n2) {
					this.result = '00000000000000000000000000000001';
				} else {
					this.result = '00000000000000000000000000000000';
				}
				break;
			}
			case 'sll': {
				// Passou no teste, mas verificar this.entry2
				// No sll, é usado um registrador e o shamt, para saber quantos bits deslocar
				// Não sei se devo usar this.entry2 como shamt
				let result = this.entry1;
				let n2 = NumberUtils.binaryToDecimal(this.entry2);
				while (n2 > 0) {
					result += '0';
					result = result.substring(1, result.length);
					n2--;
				}
				this.result = result;
				break;
			}
			case 'addi': {
				const n1 = NumberUtils.binaryToDecimal(this.entry1);
				const n2 = NumberUtils.binaryToDecimal(this.entry2);
				const num = n1 + n2;
				const aux = NumberUtils.decimalToBinary(num);
				this.result = '';
				for (let i = NUM_BITS_PROCESSOR; i > aux.length; i--) {
					this.result += '0';
				}
				this.result += aux;
				break;
			}
			case 'lw/sw': {
				const n1 = NumberUtils.binaryToDecimal(this.entry1);
				const n2 = NumberUtils.binaryToDecimal(this.entry2);
				const num = n1 + n2;
				const aux = NumberUtils.decimalToBinary(num);
				this.result = '';
				for (let i = NUM_BITS_PROCESSOR - 1; i > aux.length; i--) {
					this.result += '0';
				}
				this.result += aux;
				break;
			}
			case 'beq': {
				const n1 = NumberUtils.binaryToDecimal(this.entry1);
				const n2 = NumberUtils.binaryToDecimal(this.entry2);
				if (n1 === n2) {
					this.zero = true;
				} else {
					this.zero = false;
				}
				break;
			}
			case 'bne': {
				const n1 = NumberUtils.binaryToDecimal(this.entry1);
				const n2 = NumberUtils.binaryToDecimal(this.entry2);
				if (n1 === n2) {
					this.zero = false;
				} else {
					this.zero = true;
				}
				break;
			}
			default:
				break;
		}
		return this.result;
	}
}
