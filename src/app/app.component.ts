/* eslint-disable class-methods-use-this */
import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';
import { MipsProcessor } from './units/mips-processor.model';
import { FileUtils } from './utils/file.utils';
import { RegexUtils } from './utils/regex.utils';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	providers: [MessageService],
})
export class AppComponent {
	hasValidInstructions = false;

	inputFileHasError = false;

	instructions: string[] = [];

	isFinished = false;

	/**
	 * Tamanho da memória de instruções em bytes
	 */
	instructionMemorySize = 100;

	/**
	 * Tamanho da memória de dados em bytes
	 */
	dataMemorySize = 128; // Bytes

	mipsProcessor: MipsProcessor;

	numRegisters = 32; // bits

	/**
	 * Text to file
	 */
	text: string[] = [];

	constructor(private messageService: MessageService) {
		this.mipsProcessor = new MipsProcessor(
			this.instructionMemorySize,
			this.dataMemorySize,
			this.numRegisters,
		);
	}

	/**
	 * Realiza a leitura do arquivo de entrada e inicializa as instruções MIPS
	 * @param event
	 */
	handleFile(event: Event): void {
		const target = event.target as HTMLInputElement;
		const files = target.files as FileList;
		const file: File = files[0];
		const myReader: FileReader = new FileReader();
		myReader.onloadend = () => {
			if (myReader.result) {
				const array = myReader.result?.toString().split('\n');
				this.initInstructions(array);
			} else {
				this.inputFileHasError = true;
			}
		};
		myReader.readAsText(file);
	}

	/**
	 * Realiza a leitura do arquivo de entrada
	 * Disponibiliza as instruções válidas na variável da classe `instructions`
	 * Printa no console as instruções inválidas
	 * @param fileLines - array que contém cada uma das linhas do arquivo de entrada
	 */
	initInstructions(fileLines: string[]): void {
		let hasErrors = false;
		this.instructions = [];
		const wrongInstructions: string[] = [];

		fileLines.forEach((instruction) => {
			const aux = instruction.trim();
			if (RegexUtils.mips.test(aux)) {
				this.instructions.push(aux);
			} else {
				wrongInstructions.push(instruction);
				hasErrors = true;
			}
		});

		if (hasErrors) {
			this.messageService.add({
				severity: 'warn',
				summary: 'Leitura de Arquivo',
				detail:
					'O arquivo foi carregado, porém instruções com erro foram detectadas e descartadas',
				life: 5000,
			});
			console.log('Instruções com erro:', wrongInstructions);
		} else {
			this.messageService.add({
				severity: 'success',
				summary: 'Leitura de Arquivo',
				detail: 'O arquivo de instruções foi carregado com sucesso',
			});
		}

		if (this.instructions.length > 0) {
			this.hasValidInstructions = true;
			this.reset();
		} else {
			this.hasValidInstructions = false;
		}
	}

	nextStep(): void {
		// Adicionar Ação para "passo-a-passo"
		// console.log('avancei');
		this.isFinished = !this.mipsProcessor.run();

		if (this.isFinished) {
			this.messageService.add({
				severity: 'success',
				summary: 'Execução das Instruções',
				detail: 'A execução das instruções terminou',
			});
		}
	}

	directExecution(): void {
		// Adicionar Ação para "execução direta"
		while (!this.isFinished) {
			this.isFinished = !this.mipsProcessor.run();
		}

		if (this.isFinished) {
			this.messageService.add({
				severity: 'success',
				summary: 'Execução das Instruções',
				detail: 'A execução das instruções terminou',
			});
		}
	}

	downloadFile(): void {
		FileUtils.appendText(this.text, FileUtils.workHeader());
		FileUtils.appendText(this.text, this.mipsProcessor.logs);
		FileUtils.writeTextToFile(this.text);
		this.text = []; // clear text after write

		this.messageService.add({
			severity: 'success',
			summary: 'Arquivo de Informações',
			detail: 'O arquivo de informações de valores foi gerado com sucesso',
		});
	}

	reset(): void {
		this.mipsProcessor = new MipsProcessor(
			this.instructionMemorySize,
			this.dataMemorySize,
			this.numRegisters,
		);
		this.mipsProcessor.loadInstructionsFromFile(this.instructions);
		this.isFinished = false;
	}
}
