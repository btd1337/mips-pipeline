# Mips Single Cycle

Este projeto foi desenvolvido utilizando o framework [Angular](https://angular.io/) em sua versão `12`, utilizando a linguagem [Typescript](https://www.typescriptlang.org/) em sua versão `4.3`.

> Guilherme Justen e Helder Bertoldo

## Acessar Online

Este projeto pode ser acessado através de https://btd1337.gitlab.io/mips-pipeline/

### Arquivo de exemplo

Um arquivo de exemplo está disponível na pasta raíz para testes: `demo.input.txt`

## Acessar localmente:

### Requisítos

Para executá-lo é necessário ter instalado o [Node.js](https://nodejs.org/en/) e o [Node Package](https://www.npmjs.com/package/npm)

### Dependências

Instalar as dependências do projeto

```
npm install
```

### Rodar

Executar

```
npm start
```

Abra o navegador em http://localhost:4200/.
